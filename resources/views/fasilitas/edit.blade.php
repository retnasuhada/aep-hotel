@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">@yield('content-title')</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Fasilitas</a></li>
                        <li class="breadcrumb-item active">Tambah</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card card-info">
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="{{ route('fasilitas.update') }}" enctype="multipart/form-data" method="POST">
                        @csrf
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 col-form-label">Nama Fasilitas</label>
                                <div class="col-sm-9">
                                    <input type="hidden" name="id" value="{{$old->id}}">
                                    <input type="text" class="form-control" name="nama_fasilitas" id="inputEmail3" value="{{$old->nama_fasilitas}}">
                                    @if ($errors->has('nama_fasilitas'))
                                    <span class="text-danger">{{ $errors->first('nama_fasilitas') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-3 col-form-label">Jenis Fasilitas</label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="jenis_fasilitas">
                                        <option value="">- Pilih Jenis Fasilitas-</option>
                                        <option value="Kamar" <?php if($old->jenis_fasilitas=='Kamar'){ echo "selected";} ?>>Kamar</option>
                                        <option value="Hotel" <?php if($old->jenis_fasilitas=='Hotel'){ echo "selected";} ?>>Hotel</option>
                                    </select>
                                    @if ($errors->has('jenis_fasilitas'))
                                    <span class="text-danger">{{ $errors->first('jenis_fasilitas') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 col-form-label">Icon</label>
                                <div class="col-sm-9">
                                    <div class="custom-file">
                                        <input type="file" name="icon" class="custom-file-input" id="customFile">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                    @if ($errors->has('icon'))
                                    <span class="text-danger">{{ $errors->first('icon') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 col-form-label">Gambar</label>
                                <div class="col-sm-9">
                                    <div class="custom-file">
                                        <input type="file" name="gambar" class="custom-file-input" id="customFile">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                    @if ($errors->has('gambar'))
                                    <span class="text-danger">{{ $errors->first('gambar') }}</span>
                                    @endif
                                </div>
                            </div>


                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-info">Simpan</button>
                            <a href="{{url('fasilitas')}}" class="btn btn-warning">Kembali</a>

                            <button type="reset" class="btn btn-danger ">Batal</button>
                        </div>
                        <!-- /.card-footer -->
                    </form>

                </div>
            </div>

        </div>
    </div>
</div>

</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
    </div>
</aside>

@endsection