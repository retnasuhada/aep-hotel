@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">@yield('content-title')</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Starter Page</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <div class="container">
        <div class="row justify-content-center">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">List Fasilitas Hotel  <p style="font-size:14px; color:red;">* Untuk melakukan perubahan dan penambahan data di menu Kamar -> Fasilitas</p></h3>
                        
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>icon</th>
                                    <th>Nama Fasilitas</th>
                                    <th>Jenis Fasilitas</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                @php 
                                $no=0;
                                @endphp
                               @foreach($data as $row)
                               @php 
                                $no++;
                                @endphp
                                <tr>
                                    <td>{{$no}}</td>
                                    <td><img style="width: 50px; height:50px;" src="{{asset('images/'.$row['icon'])}}"></td>
                                    <td>{{$row['nama_fasilitas']}}</td>
                                    <td>{{$row['jenis_fasilitas']}}</td>
                                   
                                </tr>
                               @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>

        </div>
    </div>
</div>

</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
    </div>
</aside>

@endsection