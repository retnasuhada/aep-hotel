@extends('layouts.app-tamu')
@section('content')
<!--================ Accomodation Area  =================-->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<style type="text/css">
    table tr td,
    table tr th {
        font-size: 9pt;
    }

</style>

<!--================ Facilities Area  =================-->
<section class="facilities_area section_gap">
    <section class="button-area">
        <div class="container border-top-generic">

            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card card-info">
                        <!-- /.card-header -->
                        <!-- form start -->
                        @if($errors->any())
                        <h5 style="color:red;">{{$errors->first()}}</h5>
                        @endif
                        <div class="card-body">
                            <p class="login-box-msg">Edit Data Diri Anda</p>

                            <form method="POST" enctype="multipart/form-data" action="{{ route('profil.update') }}">

                                @csrf
                                <input type="hidden" name="role" value="1">
                                <div class="row mb-3">
                                    <label for="nik" class="col-md-4 col-form-label text-md-end">{{ __('NIK') }}</label>

                                    <div class="col-md-8">
                                        <input id="nik" type="text" class="form-control" name="nik" required value={{$data->nik}} autocomplete="nik" autofocus>

                                        @error('nik')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <label for="nama" class="col-md-4 col-form-label text-md-end">{{ __('Nama') }}</label>

                                    <div class="col-md-8">
                                        <input id="nama" type="text" class="form-control @error('nama') is-invalid @enderror" value="{{$data->nama}}" name="nama" required autocomplete="nama" autofocus>

                                        @error('nama')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <label for="jenis_kelamin" class="col-md-4 col-form-label text-md-end">{{ __('Gender') }}</label>

                                    <div class="col-md-8">
                                        <select name="jenis_kelamin" class="form-control">
                                            <option value="">-Pilih Jenis Kelamin-</option>
                                            <option value="laki-laki" <?php if($data->jenis_kelamin=='laki-laki'){echo "selected";} ?>>Laki-Laki</option>
                                            <option value="perempuan" <?php if($data->jenis_kelamin=='perempuan'){echo "selected";} ?>>Perempuan</option>
                                        </select>
                                        @error('jenis_kelamin')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <label for="alamat" class="col-md-4 col-form-label text-md-end">{{ __('Alamat') }}</label>

                                    <div class="col-md-8">
                                        <textarea name="alamat" class="form-control">{{$data->alamat}}</textarea>

                                        @error('alamat')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <label for="telepon" class="col-md-4 col-form-label text-md-end">{{ __('telepon') }}</label>

                                    <div class="col-md-8">
                                        <input id="phone" type="text" class="form-control @error('telepon') is-invalid @enderror" name="telepon" value="{{$data->telepon}}" required autocomplete="phone" autofocus>

                                        @error('telepon')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>


                                <div class="row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Simpan') }}
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>

</section>
<!--================ Accomodation Area  =================-->
@endsection
