@extends('layouts.app-tamu')
@section('content')
<!--================ Accomodation Area  =================-->


<!--================ Facilities Area  =================-->
<section class="facilities_area section_gap">
    <div class="overlay bg-parallax" data-stellar-ratio="0.8" data-stellar-vertical-offset="0" data-background="">
    </div>
    <div class="container">
        <div class="section_title text-center">
            <h2 class="title_w">Fasilitas Hotel </h2>
            @if($errors->any())
            <h4 style="color:red;">{{$errors->first()}}</h4>
            {{-- <input type="hidden" value="error" id="{{$errors->first()}}"> --}}
            @endif
        </div>
        <div class="row mb_30">
            @foreach($data['fasilitas_hotel'] as $f_hotel)
            <div class="facilities_item">
                <h4 class="sec_h4"><i class="lnr lnr-dinner"></i>{{$f_hotel['nama_fasilitas']}}</h4>

            </div>

            @endforeach

        </div>
    </div>
</section>
<!--================ Facilities Area  =================-->
<section class="about_history_area section_gap">
    <div class="container">
        <div class="row">
            <div class="col-md-6 d_flex align-items-center">
                <div class="about_content ">
                    <h2 class="title title_color">{{$data['kamar']['nama_kamar']}}</h2>
                    {{-- {{$data['kamar']['foto']}} --}}
                </div>
            </div>
            <div class="col-md-6">
                <img class="img-fluid" src="{{ asset('images/'.$data['kamar']['foto']['foto'])}}" alt="img">
            </div>
        </div>
    </div>
</section>
<!--================ About History Area  =================-->
<!--================Booking Tabel Area =================-->
<section class="hotel_booking_area">
    <div class="container">
        <div class="row hotel_booking_table">
            <div class="col-md-2">
                <h2>Booking<br></h2>
            </div>
            <form class="form-horizontal" action="{{ route('tamu.kamar.booking') }}" enctype="multipart/form-data" method="POST">
                @csrf
                <div class="col-md-12">
                    <div class="boking_table">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="book_tabel_item">
                                    <div class="form-group">
                                        <div class='input-group date'>
                                            <input type='date' name="start_date" class="form-control" placeholder="Start Date" />

                                            <input type='hidden' name="kamar_id" class="form-control" value="{{$data['kamar']['id']}}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="book_tabel_item">
                                    <div class="form-group">
                                        <div class='input-group date'>
                                            <input type='date' name="end_date" class="form-control" placeholder="Start Date" />

                                            <input type='hidden' class="form-control" value="{{$data['kamar']['id']}}" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="book_tabel_item">
                                    <div class="input-group">
                                        <input type='number' name='jumlah' class="form-control" placeholder="Jumlah" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="book_tabel_item">
                                    <button type="submit" class="book_now_btn button_hover"> BOOKING</button>
                                    {{-- <a class="book_now_btn button_hover" href="{{url('tamu/kamar/booking/'.$data['kamar']['id'])}}">Book Now</a> --}}
                                </div>
                            </div>
                        </div>
                    </div>
            </form>
        </div>
    </div>
    </div>
</section>
<!--================Booking Tabel Area  =================-->
<!--================ Testimonial Area  =================-->
<section class="testimonial_area section_gap">
    <div class="container">
        <div class="section_title text-center">
            <h2 class="title_color">Testimonial Kamar Ini</h2>
        </div>
        <div class="testimonial_slider owl-carousel">
            @foreach($data['review'] as $row)
            <div class="media testimonial_item">
                <img class="rounded-circle" src="image/testtimonial-1.jpg" alt="">
                <div class="media-body">
                    <p>{{$row['review']}}</p>
                    <a href="#">
                        <h4 class="sec_h4">{{$row['tamu']['nama']}}</h4>
                    </a>
                    @if($row['star']=='1')
                    <div class="star">
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star-o"></i></a>
                        <a href="#"><i class="fa fa-star-o"></i></a>
                        <a href="#"><i class="fa fa-star-o"></i></a>
                        <a href="#"><i class="fa fa-star-o"></i></a>
                    </div>
                    @endif
                    @if($row['star']=='2')
                    <div class="star">
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star-o"></i></a>
                        <a href="#"><i class="fa fa-star-o"></i></a>
                        <a href="#"><i class="fa fa-star-o"></i></a>
                    </div>
                    @endif
                    @if($row['star']=='3')
                    <div class="star">
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star-o"></i></a>
                        <a href="#"><i class="fa fa-star-o"></i></a>
                    </div>
                    @endif
                    @if($row['star']=='4')
                    <div class="star">
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star-o"></i></a>
                    </div>
                    @endif
                    @if($row['star']=='5')
                    <div class="star">
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                    </div>
                    @endif

                </div>
            </div>
            @endforeach

        </div>
    </div>
</section>
<!--================ Accomodation Area  =================-->
@endsection
