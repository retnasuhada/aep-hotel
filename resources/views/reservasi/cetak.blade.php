<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>AdminLTE 3 | Invoice Print</title>

    <!-- Google Font: Source Sans Pro -->
    {{-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback"> --}}
    <!-- Font Awesome -->
    {{-- <link rel="stylesheet" href="{{ asset('AdminLTE/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('AdminLTE/dist/css/adminlte.min.css')}}"> --}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style type="text/css">
        table tr td,
        table tr th {
            font-size: 9pt;
        }

    </style>
</head>
<body>
    <div class="wrapper">
        <!-- Main content -->
        <section class="invoice">
            <!-- title row -->
            <div class="row">
                <div class="col-12">
                    <h2 class="page-header">
                        <i class="fas fa-globe"></i> Aef-Hotel
                        <small class="float-right">Date: {{date('Y-m-d', strtotime($data['reservasi']['created_at']))}}</small>
                    </h2>
                </div>
                <!-- /.col -->
            </div>
            <!-- info row -->

            <!-- /.row -->
            <div class="row">
                <div class="col-12 table-responsive">
                    <table border="0">
                        <thead>
                            <tr>
                                <th colspan="4">Data Tamu</th>
                                <th colspan="3">Data Kamar</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="text-align:left">Nama</td>
                                <td style="text-align:right">:
                                <td>
                                <td style="text-align:left">{{$data['tamu']['nama']}}</td>

                                <td style="text-align:left">Type Kamar</td>
                                <td style="text-align:right">:
                                <td>
                                <td style="text-align:left">{{$data['kamar']['tipeKamar']['tipe_kamar']}}</td>
                            </tr>
                            <tr>
                                <td style="text-align:left">NIK</td>
                                <td style="text-align:right">:
                                <td>
                                <td style="text-align:left">{{$data['tamu']['nik']}}</td>

                                <td style="text-align:left">Nama Kamar</td>
                                <td style="text-align:right">:
                                <td>
                                <td style="text-align:left">{{$data['kamar']['nama_kamar']}}</td>
                            </tr>
                            <tr>
                                <td style="text-align:left">Jenis Kelamin</td>
                                <td style="text-align:right">:
                                <td>
                                <td style="text-align:left">{{$data['tamu']['jenis_kelamin']}}</td>

                                <td style="text-align:left">Harga Kamar</td>
                                <td style="text-align:right">:
                                <td>
                                <td style="text-align:left">{{"Rp. ".number_format($data['kamar']['harga'])}}</td>
                            </tr>
                            <tr>
                                <td style="text-align:left">Telephone</td>
                                <td style="text-align:right">:
                                <td>
                                <td style="text-align:left">{{$data['tamu']['telepon']}}</td>
                                <td style="text-align:left">Start Date</td>
                                <td style="text-align:right">:
                                <td>
                                <td style="text-align:left">{{$data['reservasi']['start_date']}}</td>

                            </tr>
                            <tr>
                                <td style="text-align:left"></td>
                                <td style="text-align:right">
                                <td>
                                <td style="text-align:left"></td>
                                <td style="text-align:left">End Date</td>
                                <td style="text-align:right">:
                                <td>
                                <td style="text-align:left">{{$data['reservasi']['end_date']}}</td>

                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>
            <!-- Table row -->
            <div class="row">
                <div class="col-12 table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tipe Kamar</th>
                                <th>Nama Kamar</th>
                                <th>Harga</th>
                                <th>Qty Kamar</th>
                                <th>Lama</th>
                                <th>Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>{{$data['kamar']['tipeKamar']['tipe_kamar']}}</td>
                                <td>{{$data['kamar']['nama_kamar']}}</td>
                                <td>{{"Rp. ".number_format($data['kamar']['harga'])}}</td>
                                <td>{{$data['reservasi']['qty_kamar']}}</td>
                                <td>{{$data['reservasi']['lama']}}</td>
                                <td>
                                    <?php
                      $sub =$data['kamar']['harga'] * $data['reservasi']['qty_kamar'] * $data['reservasi']['lama']; 
                      ?>
                                    {{"Rp. ".number_format($sub)}}
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->


            <!-- /.col -->
            <div class="col-6">


                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th style="width:50%">Subtotal:</th>
                            <td> {{"Rp. ".number_format($sub)}} </td>
                        </tr>

                        <tr>
                            <th>Total:</th>
                            <td>{{"Rp. ".number_format($sub)}}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- /.col -->
    </div>
    <!-- /.row -->
    </section>
    <!-- /.content -->
    </div>
    <!-- ./wrapper -->
    <!-- Page specific script -->
    <script>
        window.addEventListener("load", window.print());

    </script>
</body>
</html>
