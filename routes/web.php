<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });


Route::get('/', [App\Http\Controllers\BaseController::class, 'index'])->name('index');


Route::get('/login', function(){
    return view('login')->name('login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



Route::group(['prefix' =>'kamar'], function (){
    Route::get('/','KamarController@index')->name('kamar.index');
    Route::get('/add','KamarController@add')->name('kamar.add');
    Route::get('/{id}','KamarController@edit')->name('kamar.edit');
    Route::get('/show/{id}','KamarController@show')->name('kamar.show');
    Route::get('/fasilitas/add/{id}','KamarController@addfasilitas')->name('kamar.fasilitas.add');
    Route::get('/fasilitas/delete/fasilitas/{id}','KamarController@deletefasilitas')->name('kamar.fasilitas.delete');
    Route::post('/','KamarController@store')->name('kamar.store');
    Route::post('/update','KamarController@update')->name('kamar.update');
    Route::post('/delete','KamarController@delete')->name('kamar.delete');
    Route::post('/fasilitas/store','KamarController@storefasilitas')->name('kamar.fasilitas.store');

    Route::get('/galery/add/{id}','KamarController@addGalery')->name('kamar.galery.add');
    Route::get('/galery/delete/{id}','KamarController@deleteGalery')->name('kamar.galery.delete');
    Route::post('/galery/store','KamarController@storeGalery')->name('kamar.galery.store');
});


Route::group(['prefix' =>'profil'], function (){
    Route::get('/','ProfilController@index')->name('profil');
    Route::get('/password','ProfilController@password')->name('profil.password');
    Route::get('/edit','ProfilController@edit')->name('profil.edit');
    Route::post('/update/profil','ProfilController@update')->name('profil.update');
    Route::post('/password/update','ProfilController@passwordUpdate')->name('profil.update-password');
});



Route::group(['prefix' =>'tipe'], function (){
    Route::get('/','TipeController@index')->name('tipe.index');
    Route::get('/{id}','TipeController@edit')->name('tipe.edit');
    Route::post('/','TipeController@store')->name('tipe.store');
    Route::post('/update','TipeController@update')->name('tipe.update');
    Route::post('/delete','TipeController@delete')->name('tipe.delete');
});

Route::group(['prefix' =>'user'], function (){
    Route::get('/','UserController@index')->name('user.index');
    Route::get('/{id}','UserController@edit')->name('user.edit');
    Route::post('/','UserController@store')->name('user.store');
    Route::post('/update','UserController@update')->name('user.update');
});

Route::group(['prefix' =>'tamu'], function (){
    Route::get('/kamar/show/{id}','BaseController@show')->name('tamu.kamar.show');
    Route::post('/kamar/booking','BaseController@booking')->name('tamu.kamar.booking');
    Route::get('/invoice/{id}','BaseController@invoice')->name('tamu.invoice');
    Route::get('/rating/{id}','BaseController@rating')->name('tamu.rating');
    Route::get('/pesanan','BaseController@pesanan')->name('tamu.pesanan');
    Route::post('/rating','BaseController@storeRating')->name('rating.store');
});

Route::group(['prefix' =>'reservasi'], function (){
    Route::get('/','ReservasiController@index')->name('reservasi.index');
    Route::get('/{id}','ReservasiController@show')->name('reservasi.show');
    Route::get('/download/{id}','ReservasiController@download')->name('reservasi.download');
    Route::get('/checkin/{id}','ReservasiController@checkin')->name('reservasi.checkin');
    Route::get('/checkout/{id}','ReservasiController@checkout')->name('reservasi.checkout');
    Route::get('/cancel/{id}','ReservasiController@cancel')->name('reservasi.cancel');
    Route::post('/','ReservasiController@store')->name('reservasi.store');
    Route::post('/update','ReservasiController@update')->name('reservasi.update');
});


Route::group(['prefix' =>'fasilitas'], function (){
    Route::get('/','FasilitasController@index')->name('fasilitas.index');
    Route::get('/add','FasilitasController@add')->name('fasilitas.add');
    Route::get('/{id}','FasilitasController@show')->name('fasilitas.show');

    Route::post('/','FasilitasController@store')->name('fasilitas.store');
    Route::post('/update','FasilitasController@update')->name('fasilitas.update');
    Route::post('/delete','FasilitasController@delete')->name('fasilitas.delete');
});

Route::group(['prefix' =>'fasilitas-hotel'], function (){
    Route::get('/','FasilitasHotelController@index')->name('fasilitas-hotel.index');
    Route::get('/add','FasilitasController@addHotel')->name('fasilitas-hotel.add');
});



