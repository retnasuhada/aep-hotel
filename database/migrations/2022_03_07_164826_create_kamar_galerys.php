<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKamarGalerys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kamar_galerys', function (Blueprint $table) {
            $table->id();
            $table->text('url')->nullable();
            $table->string('foto')->nullable();
            $table->boolean('is_active')->default(false);
            $table->integer('kamar_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kamar_galerys');
    }
}
