<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservasis', function (Blueprint $table) {
            $table->id();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->integer('lama')->nullable();
            $table->integer('qty_kamar')->nullable();
            $table->enum('status',['RESERVASI','CHECKIN','CHECKOUT','CANCEL'])->nullable();
            $table->integer('approved_by')->nullable();
            $table->integer('canceled_by')->nullable();
            $table->dateTime('approved_date')->nullable();
            $table->dateTime('canceled_date')->nullable();
            $table->integer('tamu_id')->nullable();
            $table->integer('kamar_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservasi');
    }
}
