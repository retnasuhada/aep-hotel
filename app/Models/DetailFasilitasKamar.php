<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DetailFasilitasKamar extends Model
{
    use HasFactory;

    protected $table = 'detail_fasilitas_kamars';
    protected $fillable = [
        'fasilitas_id',
        'kamar_id',
    ];

    public function fasilitas()
    {
        return $this->belongsto('\App\Models\Fasilitas', 'fasilitas_id', 'id')->where('status',1);
    }


}
