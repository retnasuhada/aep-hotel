<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TipeKamar extends Model
{
    use HasFactory;

    protected $table = 'tipe_kamars';
    protected $fillable = [
        'tipe_kamar',
        'status'
    ];

    public function kamar(){
        return $this->hasMany('App\Models\Kamar', 'tipe_id', 'id')->where('status',1)->with('galery');
    }
}
