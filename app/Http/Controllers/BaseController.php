<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Kamar;
use App\Models\TipeKamar;
use App\Models\KamarGaleri;
use App\Models\Fasilitas;
use App\Models\DetailFasilitasKamar;
use App\Models\Tamu;
use App\Models\Review;
use App\Models\Reservasi;
use Illuminate\Support\Carbon;


class BaseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $tipe = TipeKamar::query()->where('status', 1)->whereHas('kamar')->with('kamar')->get();

        return view('index', ['data' => $tipe]);
    }

    public function rating($id)
    {
        $reservasi = Reservasi::query()->where('id',$id)->first();


        return view('ratings',['data'=> $reservasi]);
    }

    public function storeRating(Request $request)
    {
        $review = new Review;
        $review->star = $request->star;
        $review->review = $request->review;
        $review->tamu_id = $request->tamu_id;
        $review->kamar_id = $request->kamar_id;
        $review->save();

        return redirect()->route('tamu.pesanan');
        // return view('ratings');
    }

    public function show($id)
    {

        $kamar = Kamar::query()->where('id', $id)->with('foto')->first();
        $fasilitas = DetailFasilitasKamar::query()->where('kamar_id', $id)->with('fasilitas')->get();
        $review = Review::query()->with('tamu')->where('kamar_id',$kamar->id)->get();
        $f_hotels = Fasilitas::query()->where(['jenis_fasilitas'=>'Hotel','status'=>1])->get();
        $fHotel = array();
        foreach ($f_hotels as $key => $value) {
            $fHotel[$key] = [
                'id' => $value->id,
                'nama_fasilitas' => $value->nama_fasilitas,
                'icon' => $value->icon,
                'gambar' => $value->gambar
            ];
        }

        $data = [
            'kamar' => $kamar,
            'review' => $review,
            'fasilitas' => $fasilitas,
            'fasilitas_hotel' => $fHotel
        ];
        return view('detail', ['data' => $data]);
    }

    public function invoice($id)
    {
        $reservasi = Reservasi::find($id);
        $tamu = Tamu::query()->where('id', $reservasi->tamu_id)->first();
        $kamar = Kamar::query()->where('id', $reservasi->kamar_id)->with(['tipeKamar'])->first();

        $data = [
            'tamu' => $tamu,
            'kamar' => $kamar,
            'reservasi' => $reservasi,
        ];
        return view('invoice', ['data' => $data]);
    }

    public function pesanan()
    {


        $user = Auth::user();

        $tamu = Tamu::where('user_id', $user->id)->first();

        $reservasi = Reservasi::query()->where('tamu_id', $tamu->id)->with(['kamar', 'tamu'])->get();
        $data = [
            'reservasi' => $reservasi
        ];
        return view('pesanan', ['data' => $data]);
    }

    public function booking(Request $request)
    {
        if (!Auth::check()) {
            return view('auth.login');
        }
        $tgl1 = new Carbon($request->start_date);
        $tgl2 = new Carbon($request->end_date);
        $now = Carbon::now()->format('Y-m-d');

        if($tgl2 < $tgl1){
            return redirect()->back()->withErrors(['Oops!, Tanggal Akhir Tidak boleh kurang dari Tanggal Awal', 'The Message']);
        }

        if($tgl1 < $now){
            return redirect()->back()->withErrors(['Oops!, Tanggal Booking tidak boleh kurang dari tanggal sekarang', 'The Message']);
        }


        $jarak = $tgl2->diffInDays($tgl1) + 1;


        $tamu = Tamu::query()->where('user_id', Auth::user()->id)->first();
        $kamar = Kamar::query()->where('id', $request->kamar_id)->with(['tipeKamar'])->first();
        if($kamar->jumlah < $request->jumlah){
            return redirect()->back()->withErrors(['Oops!, Mohon Maaf, kamar saat ini tersedia '.$kamar->jumlah. ' Kamar', 'The Message']);
        }
        // dd($request->kamar_id);
        $reservasi = new Reservasi;
        $reservasi->start_date = $request->start_date;
        $reservasi->end_date = $request->end_date;
        $reservasi->lama = $jarak;
        $reservasi->qty_kamar = $request->jumlah;
        $reservasi->status = 'RESERVASI';
        $reservasi->kamar_id = $kamar->id;
        $reservasi->tamu_id = $tamu->id;
        $reservasi->save();

        return redirect()->route('tamu.invoice', $reservasi->id);
    }
}
