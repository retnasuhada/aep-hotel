<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Kamar;
use App\Models\TipeKamar;
use App\Models\KamarGaleri;
use App\Models\Tamu;

class TipeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $tipe = TipeKamar::query()->where('status','1')->get();

        return view('tipe.index',['data'=>$tipe,'edit'=>0]);
    }

    public function add()
    {
        $tipe = TipeKamar::query()->where('status','1')->get();
        return view('kamar.add',['data'=>$tipe,'edit'=>0]);
    }




    public function edit($id)
    {
        $tipe = TipeKamar::query()->where('status','1')->get();
        $old = TipeKamar::find($id);
        return view('tipe.index',['data'=>$tipe,'edit'=>1,'old'=>$old]);
    }


    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'tipe' => 'required|string|min:3',
            'status' => 'required',
        ], [
            'tipe.required' => 'Tipe Kamar Tidak Boleh Kosong!',
            'tipe.string' => 'Tipe Kamar Harus Berupa Teks!',
            'status.required' => 'Status Kamar Tidak Boleh Kosong',
            'tipe.min'=>'Tipe Kamar Minimal 3 Karakter'
        ]);

        $tipe = new TipeKamar;
        $tipe->tipe_kamar = $request->tipe;
        $tipe->status = $request->status;
        $tipe->save();
        return redirect()->route('tipe.index')->with('success', 'Data Berhasil Di Tambah');
    }

    public function update(Request $request)
    {
        $validatedData = $request->validate([
            'tipe' => 'required|string|min:3',
            'status' => 'required',
        ], [
            'tipe.required' => 'Tipe Kamar Tidak Boleh Kosong!',
            'tipe.string' => 'Tipe Kamar Harus Berupa Teks!',
            'status.required' => 'Status Kamar Tidak Boleh Kosong',
            'tipe.min'=>'Tipe Kamar Minimal 3 Karakter'
        ]);

        $tipe = TipeKamar::find($request->id);
        $tipe->tipe_kamar = $request->tipe;
        $tipe->status = $request->status;
        $tipe->update();
        return redirect()->route('tipe.index')->with('success', 'Data Berhasil Di Tambah');
    }

    public function delete(Request $request)
    {
        $tipe = TipeKamar::find($request->id);
        $tipe->status = 0;
        $tipe->update();
        return redirect()->route('tipe.index')->with('success', 'Data Berhasil Di Tambah');
    }
}
